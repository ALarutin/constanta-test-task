FROM ubuntu:latest as user
RUN useradd developer

FROM golang:1.16-alpine as builder
WORKDIR /go/src/gitlab/constanta-test-task
COPY ./ ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o constanta-test-task cmd/constanta-test-task/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=user /etc/passwd /etc/passwd
COPY --from=builder /go/src/gitlab/constanta-test-task/constanta-test-task constanta-test-task
COPY ./configs ./configs
USER developer
EXPOSE 8080
CMD ["./constanta-test-task"]