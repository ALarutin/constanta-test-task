package router

import (
	"net/http"
)

type (
	controller interface {
		multiplexer
	}
	multiplexer interface {
		Multiplex(w http.ResponseWriter, r *http.Request)
	}

	middleware interface {
		recoverMiddlewareGetter
		timeoutMiddlewareGetter
		requestCounter
	}
	recoverMiddlewareGetter interface {
		Recover(next http.Handler) http.Handler
	}
	timeoutMiddlewareGetter interface {
		Timeout(next http.Handler) http.Handler
	}
	requestCounter interface {
		CountRequest(next http.Handler) http.Handler
	}
)

func NewHandler(c controller, m middleware) http.Handler {
	var (
		mux = http.NewServeMux()

		middlewareFunc = func(h http.HandlerFunc) http.Handler {
			return m.Recover(m.CountRequest(m.Timeout(h)))
		}
	)

	mux.Handle("/multiplexer", middlewareFunc(c.Multiplex))

	return mux
}
