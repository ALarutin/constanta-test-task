package httpClient

import (
	"encoding/base64"
)

type mockCache struct{}

func (*mockCache) Get(string) (string, bool) {
	return base64.StdEncoding.EncodeToString([]byte(mockRespStr)), true
}
func (*mockCache) Set(string, string) {}

type mockEmptyCache struct{}

func (*mockEmptyCache) Get(string) (string, bool) {
	return "", false
}
func (*mockEmptyCache) Set(string, string) {}
