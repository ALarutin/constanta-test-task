package httpClient

type (
	cache interface {
		getter
		setter
	}
	getter interface {
		Get(key string) (string, bool)
	}
	setter interface {
		Set(key, data string)
	}
)
