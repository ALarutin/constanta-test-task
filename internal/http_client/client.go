package httpClient

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"time"
)

type (
	Client struct {
		cache  cache
		client doer
		cfg    Cfg
	}
	Cfg struct {
		DoCache bool
		Timeout time.Duration
	}
)

func NewHClient(cfg Cfg, cache cache, httpClient doer) *Client {
	return &Client{
		cache:  cache,
		client: httpClient,
		cfg:    cfg,
	}
}

func (c *Client) Do(ctx context.Context, url string) (string, error) {
	if c.cfg.DoCache {
		if body, ok := c.cache.Get(url); ok {
			return body, nil
		}
	}

	reqCtx, cancel := context.WithTimeout(ctx, c.cfg.Timeout)
	defer cancel()

	req, err := http.NewRequestWithContext(reqCtx, http.MethodGet, url, nil)
	if err != nil {
		return "", fmt.Errorf("url: %s;invalidate request url; %w", url, err)
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", fmt.Errorf("url: %s; error by requesting; %w", url, err)
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("url: %s; error by parsing response; %w", url, err)
	}

	base64Body := base64.StdEncoding.EncodeToString(body)

	if c.cfg.DoCache {
		go c.cache.Set(url, base64Body)
	}

	return base64Body, nil
}
