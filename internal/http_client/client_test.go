package httpClient

import (
	"context"
	"encoding/base64"
	"fmt"
	"reflect"
	"testing"
)

const mockUrlStr = "http://localhost:8080/"

func TestClient_Do(t *testing.T) {
	type fields struct {
		cache  cache
		client doer
		cfg    Cfg
	}
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
		errMsg  string
	}{
		{
			name: "Successful test, get from cache",
			fields: fields{
				cache:  new(mockCache),
				client: new(mockDoerSuccessful),
				cfg:    Cfg{DoCache: true},
			},
			args: args{url: mockUrlStr},
			want: base64.StdEncoding.EncodeToString([]byte(mockRespStr)),
		},
		{
			name: "Successful test, empty cache",
			fields: fields{
				cache:  new(mockEmptyCache),
				client: new(mockDoerSuccessful),
				cfg:    Cfg{DoCache: true},
			},
			args: args{url: mockUrlStr},
			want: base64.StdEncoding.EncodeToString([]byte(mockRespStr)),
		},
		{
			name: "Unsuccessful test, doer error",
			fields: fields{
				cache:  new(mockEmptyCache),
				client: new(mockDoerUnsuccessful),
				cfg:    Cfg{DoCache: true},
			},
			args:    args{url: mockUrlStr},
			wantErr: true,
			errMsg:  fmt.Sprintf("url: %s; error by requesting; %s", mockUrlStr, mockDoerError.Error()),
		},
		{
			name: "Unsuccessful test, doer error",
			fields: fields{
				cache:  new(mockEmptyCache),
				client: new(mockDoerUnsuccessfulReaderError),
				cfg:    Cfg{DoCache: true},
			},
			args:    args{url: mockUrlStr},
			wantErr: true,
			errMsg:  fmt.Sprintf("url: %s; error by parsing response; %s", mockUrlStr, mockReadError.Error()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				cache:  tt.fields.cache,
				client: tt.fields.client,
				cfg:    tt.fields.cfg,
			}
			got, err := c.Do(context.Background(), tt.args.url)
			if err != nil && !tt.wantErr {
				t.Errorf("Do() error = %+v, wantErr %+v", err, tt.wantErr)
				return
			}
			if (err != nil && tt.wantErr) && err.Error() != tt.errMsg {
				t.Errorf("Do() error = %+v, wantErr %s", err, tt.errMsg)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Do() got = %+v, want = %+v", got, tt.want)
			}
		})
	}
}
