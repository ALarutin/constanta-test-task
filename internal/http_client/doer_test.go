package httpClient

import (
	"errors"
	"io"
	"net/http"
	"strings"
)

type mockDoerSuccessful struct{}

const mockRespStr = "some response"

func (*mockDoerSuccessful) Do(*http.Request) (*http.Response, error) {
	return &http.Response{
		StatusCode: http.StatusOK,
		Body:       io.NopCloser(strings.NewReader(mockRespStr)),
	}, nil
}

type mockDoerUnsuccessful struct{}

var mockDoerError = errors.New("some error")

func (*mockDoerUnsuccessful) Do(*http.Request) (*http.Response, error) {
	return nil, mockDoerError
}

type mockDoerUnsuccessfulReaderError struct{}

var mockReadError = errors.New("some read error")

func (*mockDoerUnsuccessfulReaderError) Do(*http.Request) (*http.Response, error) {
	return &http.Response{
		StatusCode: http.StatusOK,
		Body:       io.NopCloser(new(mockReader)),
	}, nil
}

type mockReader struct{}

func (*mockReader) Read([]byte) (int, error) {
	return 0, mockReadError
}
