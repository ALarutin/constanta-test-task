package application

import (
	"context"
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"

	"gitlab/constanta-test-task/internal/cache"
	"gitlab/constanta-test-task/internal/controller"
	httpClient "gitlab/constanta-test-task/internal/http_client"
	"gitlab/constanta-test-task/internal/multiplexer"
	"gitlab/constanta-test-task/internal/router"
	"gitlab/constanta-test-task/pkg/config"
	"gitlab/constanta-test-task/pkg/logger"
	"gitlab/constanta-test-task/pkg/middleware"
)

type (
	Starter interface {
		Start()
	}

	application struct {
		opt Options

		cfg config.Application
		l   *log.Logger

		srv *http.Server
	}
)

func NewApplication(opt Options) Starter {
	var (
		cfg = config.NewConfig(fmt.Sprintf("./configs/%s.json", opt.Env))
		l   = logger.NewLogger(opt.LogOutputFilePath)
		cch = cache.NewCache(cache.Cfg{
			LiveDuration:  cfg.Application.Cache.LiveDuration.Cast(),
			CleanDuration: cfg.Application.Cache.CleanDuration.Cast(),
		})
		clt = httpClient.NewHClient(httpClient.Cfg{
			DoCache: cfg.Application.Client.DoCache,
			Timeout: cfg.Application.Client.Timeout.Cast(),
		}, cch, http.DefaultClient)
		mux = multiplexer.NewMultiplexer(multiplexer.Cfg{
			MaxUrlsSize:   cfg.Application.Multiplexer.MaxUrlsSize,
			MaxOutputCons: cfg.Application.Multiplexer.MaxOutputCons,
		}, clt)
		ctrl = controller.NewController(controller.Cfg{
			MaxUrlsSize: cfg.Application.Multiplexer.MaxUrlsSize,
		}, l, mux)
		mdl = middleware.NewMiddleware(middleware.Cfg{
			Timeout:      cfg.Application.Server.Timeout.Cast(),
			MaxInputCons: cfg.Application.Server.MaxInputCons,
		}, l)
		r = router.NewHandler(ctrl, mdl)
	)

	return &application{
		opt: opt,
		cfg: cfg.Application,
		l:   l,
		srv: &http.Server{
			Addr:    fmt.Sprintf(":%d", opt.Port),
			Handler: r,
		},
	}
}

func (a *application) Start() {
	if a.opt.IsPprof {
		a.l.Println("turn on pprof")
		go func() {
			if err := http.ListenAndServe(":6060", nil); err != nil {
				a.l.Println(err)
			}
		}()
	}

	stopChan := make(chan os.Signal, 1)

	go func() {
		if err := a.srv.ListenAndServe(); err != nil {
			a.l.Println(err)
		}
	}()

	a.l.Printf("\nstart listening on port: %d\napplication:\n\tname: %s\n\tversion: %s\n\tenv: %s\n",
		a.opt.Port, a.cfg.Name, a.cfg.Version, a.opt.Env)

	signal.Notify(stopChan, os.Interrupt, os.Kill)

	<-stopChan

	ctx, cancel := context.WithTimeout(context.Background(), a.cfg.Server.ShutdownTimeout.Cast())
	defer cancel()

	if err := a.srv.Shutdown(ctx); err != nil {
		a.l.Printf("shutting down error: %s", err.Error())
	} else {
		a.l.Println("app has shut down")
	}

	os.Exit(0)
}
