package application

type Options struct {
	Port int
	Env  string

	LogOutputFilePath string
	IsPprof           bool
}
