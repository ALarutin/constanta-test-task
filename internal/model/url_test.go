package model

import (
	"testing"
)

const (
	mockValidUrlStr   = "http://localhost:8080/"
	mockInvalidUrlStr = "some invalid url"
)

func TestUrls_Validation(t *testing.T) {
	type args struct {
		maxSize uint8
	}
	tests := []struct {
		name    string
		u       Urls
		args    args
		wantErr bool
		errMsg  string
	}{
		{
			name: "Successful test",
			u:    Urls{mockValidUrlStr},
			args: args{maxSize: 1},
		},
		{
			name:    "Unsuccessful test, too many urls",
			u:       Urls{mockValidUrlStr, mockValidUrlStr},
			args:    args{maxSize: 1},
			wantErr: true,
			errMsg:  "too many urls: 2, limit is 1",
		},
		{
			name:    "Unsuccessful test, empty urls",
			u:       Urls{},
			args:    args{maxSize: 1},
			wantErr: true,
			errMsg:  ErrorEmptyUrls.Error(),
		},
		{
			name:    "Unsuccessful test, not valid url",
			u:       Urls{mockInvalidUrlStr},
			args:    args{maxSize: 1},
			wantErr: true,
			errMsg:  "url number 1 is not valid",
		},
		{
			name:    "Unsuccessful test, not valid url",
			u:       Urls{mockValidUrlStr, mockValidUrlStr},
			args:    args{maxSize: 2},
			wantErr: true,
			errMsg:  "url number 2 is a duplicate",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.u.Validation(tt.args.maxSize)
			if err != nil && !tt.wantErr {
				t.Errorf("Validation() error = %+v, wantErr %+v", err, tt.wantErr)
				return
			}
			if (err != nil && tt.wantErr) && err.Error() != tt.errMsg {
				t.Errorf("Validation() error = %+v, wantErr %s", err, tt.errMsg)
				return
			}
		})
	}
}
