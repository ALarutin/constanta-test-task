package model

import (
	"errors"
	"fmt"
	urlpkg "net/url"
)

type (
	Urls []url
	url  string
)

var ErrorEmptyUrls = errors.New("urls are empty")

func (u Urls) Validation(maxSize uint8) error {
	if len(u) > int(maxSize) {
		return fmt.Errorf("too many urls: %d, limit is %d", len(u), maxSize)
	}
	if len(u) == 0 {
		return ErrorEmptyUrls
	}

	urlsMap := make(map[url]bool)

	for i, uu := range u {
		if err := uu.Validation(); err != nil {
			return fmt.Errorf("url number %d is not valid", i+1)
		}

		if _, ok := urlsMap[uu]; ok {
			return fmt.Errorf("url number %d is a duplicate", i+1)
		}

		urlsMap[uu] = true
	}

	return nil
}

func (u url) Validation() error {
	if _, err := urlpkg.ParseRequestURI(string(u)); err != nil {
		return err
	}
	return nil
}
