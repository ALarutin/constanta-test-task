package controller

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

const (
	mockValidUrlStrJson   = `["http://localhost:8080/"]`
	mockInvalidUrlStrJson = `["http://localhost:8080/]`
)

func TestController_Multiplex(t *testing.T) {
	type fields struct {
		logger *log.Logger
		cfg    Cfg
		mux    multiplexer
	}
	type args struct {
		w *httptest.ResponseRecorder
		r *http.Request
	}
	type expected struct {
		status int
		body   string
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		expected expected
	}{
		{
			name: "Successful test",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
				cfg:    Cfg{MaxUrlsSize: 1},
				mux:    new(mockMultiplexerSuccessful),
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest(http.MethodPost, "/", io.NopCloser(strings.NewReader(mockValidUrlStrJson))),
			},
			expected: expected{
				status: http.StatusOK,
				body:   fmt.Sprintf(`["%s"]`, mockRespStr),
			},
		},
		{
			name: "Unsuccessful test, wrong method",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusMethodNotAllowed,
			},
		},
		{
			name: "Unsuccessful test, wrong unmarshal",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
				cfg:    Cfg{MaxUrlsSize: 1},
				mux:    new(mockMultiplexerSuccessful),
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest(http.MethodPost, "/", io.NopCloser(strings.NewReader(mockInvalidUrlStrJson))),
			},
			expected: expected{
				status: http.StatusBadRequest,
				body:   `{"error":"invalidate json; parsing error: unexpected end of JSON input"}`,
			},
		},
		{
			name: "Unsuccessful test, multiplexer error",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
				cfg:    Cfg{MaxUrlsSize: 1},
				mux:    new(mockMultiplexerUnsuccessful),
			},
			args: args{
				w: httptest.NewRecorder(),
				r: httptest.NewRequest(http.MethodPost, "/", io.NopCloser(strings.NewReader(mockValidUrlStrJson))),
			},
			expected: expected{
				status: http.StatusBadRequest,
				body:   fmt.Sprintf(`{"error":"%s"}`, mockMultiplexerError.Error()),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Controller{
				logger: tt.fields.logger,
				cfg:    tt.fields.cfg,
				mux:    tt.fields.mux,
			}
			c.Multiplex(tt.args.w, tt.args.r)
			if tt.args.w.Code != tt.expected.status {
				t.Errorf("Multiplex() got = %d, want %d", tt.args.w.Code, tt.expected.status)
			}
			if string(tt.args.w.Body.Bytes()) != tt.expected.body {
				t.Errorf("Multiplex() got = %s, want %s", tt.args.w.Body.Bytes(), tt.expected.body)
			}
		})
	}
}
