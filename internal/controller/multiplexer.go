package controller

import (
	"context"

	"gitlab/constanta-test-task/internal/model"
)

type multiplexer interface {
	Multiplex(ctx context.Context, urls model.Urls) (model.Responses, error)
}
