package controller

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab/constanta-test-task/internal/model"
	appError "gitlab/constanta-test-task/pkg/error"
)

type (
	Controller struct {
		logger *log.Logger
		cfg    Cfg
		mux    multiplexer
	}

	Cfg struct {
		MaxUrlsSize uint8
	}
)

func NewController(cfg Cfg, logger *log.Logger, mux multiplexer) *Controller {
	return &Controller{
		logger: logger,
		cfg:    cfg,
		mux:    mux,
	}
}

const (
	kb        = 1 << 10
	maxUrlLen = 2 * kb
)

func (c *Controller) Multiplex(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		c.writeError(w, nil, http.StatusMethodNotAllowed)
		return
	}

	bytes, err := io.ReadAll(io.LimitReader(r.Body, maxUrlLen*int64(c.cfg.MaxUrlsSize)+kb))
	if err != nil {
		c.handleError(w, appError.NewBusinessError(fmt.Errorf("invalidate request body; reading error: %s", err.Error())))
		return
	}
	defer r.Body.Close()

	urls := make(model.Urls, 0, c.cfg.MaxUrlsSize)

	if err = json.Unmarshal(bytes, &urls); err != nil {
		c.handleError(w, appError.NewBusinessError(fmt.Errorf("invalidate json; parsing error: %s", err.Error())))
		return
	}

	responses, err := c.mux.Multiplex(r.Context(), urls)
	if err != nil {
		c.handleError(w, err)
		return
	}

	bytes, err = json.Marshal(responses)
	if err != nil {
		c.handleError(w, appError.NewServerError(fmt.Errorf("invalidate responses; marshaling error: %s", err.Error())))
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	w.WriteHeader(http.StatusOK)

	if _, err = w.Write(bytes); err != nil {
		c.logger.Printf("can't write into response with error: %s\n", err.Error())
	}
}

type BusinessError struct {
	Error string `json:"error"`
}

func (c *Controller) handleError(w http.ResponseWriter, err error) {
	c.logger.Println(err.Error())

	switch err.(type) {
	case *appError.BusinessError:
		errMsg, _err := json.Marshal(&BusinessError{Error: err.Error()})
		if _err != nil {
			c.logger.Println(fmt.Sprintf("can't marshal BusinessError: %s", _err.Error()))
			c.writeError(w, nil, http.StatusInternalServerError)
			return
		}

		c.writeError(w, errMsg, http.StatusBadRequest)
	default:
		c.writeError(w, nil, http.StatusInternalServerError)
	}
}

func (c *Controller) writeError(w http.ResponseWriter, errMsg []byte, code int) {
	w.WriteHeader(code)

	if len(errMsg) == 0 {
		return
	}

	w.Header().Set("Content-Type", "application/problem+json; charset=utf-8")

	if _, err := w.Write(errMsg); err != nil {
		c.logger.Printf("can't write response body with error: %s\n", err.Error())
	}
}
