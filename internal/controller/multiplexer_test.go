package controller

import (
	"context"
	"errors"

	"gitlab/constanta-test-task/internal/model"
	appError "gitlab/constanta-test-task/pkg/error"
)

type mockMultiplexerSuccessful struct{}

const mockRespStr = "some response"

func (*mockMultiplexerSuccessful) Multiplex(ctx context.Context, urls model.Urls) (model.Responses, error) {
	return model.Responses{mockRespStr}, nil
}

type mockMultiplexerUnsuccessful struct{}

var mockMultiplexerError = errors.New("some error")

func (*mockMultiplexerUnsuccessful) Multiplex(ctx context.Context, urls model.Urls) (model.Responses, error) {
	return nil, appError.NewBusinessError(mockMultiplexerError)
}
