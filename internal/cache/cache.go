package cache

import (
	"sync"
	"time"
)

type (
	Cache struct {
		cfg Cfg

		data map[string]*cacheObj

		mu *sync.Mutex
	}

	Cfg struct {
		LiveDuration  time.Duration
		CleanDuration time.Duration
	}

	cacheObj struct {
		data       string
		createTime time.Time
	}
)

func NewCache(cfg Cfg) *Cache {
	c := &Cache{
		cfg:  cfg,
		data: make(map[string]*cacheObj),
		mu:   new(sync.Mutex),
	}

	go c.clean()

	return c
}

func (c *Cache) Get(key string) (string, bool) {
	c.mu.Lock()
	obj, ok := c.data[key]
	c.mu.Unlock()

	if ok && obj.createTime.Add(c.cfg.LiveDuration).After(time.Now()) {
		return obj.data, true
	}

	return "", false
}

func (c *Cache) Set(key, data string) {
	c.mu.Lock()
	c.data[key] = &cacheObj{
		data:       data,
		createTime: time.Now(),
	}
	c.mu.Unlock()
}

func (c *Cache) clean() {
	ticker := time.NewTicker(c.cfg.CleanDuration)
	defer ticker.Stop()

	for ; true; <-ticker.C {
		for k, obj := range c.data {
			if obj.createTime.Add(c.cfg.LiveDuration).Before(time.Now()) {
				c.mu.Lock()
				delete(c.data, k)
				c.mu.Unlock()
			}
		}
	}
}
