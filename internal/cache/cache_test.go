package cache

import (
	"sync"
	"testing"
	"time"
)

const (
	mockKey  = "some key"
	mockData = "some data"
)

func TestCache_Get(t *testing.T) {
	type fields struct {
		cfg  Cfg
		data map[string]*cacheObj
		mu   *sync.Mutex
	}
	type args struct {
		key string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
		want1  bool
	}{
		{
			name: "Successful test",
			fields: fields{
				cfg: Cfg{
					LiveDuration: time.Hour,
				},
				data: map[string]*cacheObj{
					mockKey: {
						data:       mockData,
						createTime: time.Now(),
					},
				},
				mu: new(sync.Mutex),
			},
			args:  args{key: mockKey},
			want:  mockData,
			want1: true,
		},
		{
			name: "Successful test, expired data",
			fields: fields{
				cfg: Cfg{
					LiveDuration: time.Second,
				},
				data: map[string]*cacheObj{
					mockKey: {
						data:       mockData,
						createTime: time.Now().Add(-2 * time.Second),
					},
				},
				mu: new(sync.Mutex),
			},
			args: args{key: mockKey},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Cache{
				cfg:  tt.fields.cfg,
				data: tt.fields.data,
				mu:   tt.fields.mu,
			}
			got, got1 := c.Get(tt.args.key)
			if got != tt.want {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Get() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
