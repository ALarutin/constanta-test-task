package multiplexer

import (
	"context"
	"fmt"
	"sync"

	"gitlab/constanta-test-task/internal/model"
	appError "gitlab/constanta-test-task/pkg/error"
)

type worker struct {
	outCons int
	wg      *sync.WaitGroup

	client doer

	responses []string
}

func (w *worker) do(ctx context.Context, urls []urlInfo) (model.Responses, error) {
	batchCount := (len(urls) / w.outCons) + 1

	for i := 0; i < batchCount; i++ {
		var (
			first = i * w.outCons
			last  = first + w.outCons
		)

		if last > len(urls) {
			last = len(urls)
		}

		err := func() error {
			errChan := make(chan error, batchCount)

			for _, u := range urls[first:last] {
				w.wg.Add(1)

				go func(_u urlInfo) {
					defer w.wg.Done()

					resp, err := w.client.Do(ctx, _u.str)
					if err != nil {
						errChan <- fmt.Errorf("url: %s\nerror message: %s", _u.str, err.Error())
						return
					}

					w.responses[_u.position] = resp
				}(u)
			}

			w.wg.Wait()

			close(errChan)

			for err := range errChan {
				return err
			}

			return nil
		}()

		if err != nil {
			return nil, appError.NewBusinessError(err)
		}
	}

	return w.responses, nil
}
