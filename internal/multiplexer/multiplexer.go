package multiplexer

import (
	"context"
	"sync"

	"gitlab/constanta-test-task/internal/model"
	appError "gitlab/constanta-test-task/pkg/error"
)

type (
	Multiplexer struct {
		cfg    Cfg
		client doer
	}

	Cfg struct {
		MaxUrlsSize   uint8
		MaxOutputCons int
	}
)

func NewMultiplexer(cfg Cfg, client doer) *Multiplexer {
	return &Multiplexer{
		cfg:    cfg,
		client: client,
	}
}

func (m *Multiplexer) Multiplex(ctx context.Context, inUrls model.Urls) (model.Responses, error) {
	if err := inUrls.Validation(m.cfg.MaxUrlsSize); err != nil {
		return nil, appError.NewBusinessError(err)
	}

	w := &worker{
		outCons:   m.cfg.MaxOutputCons,
		responses: make([]string, len(inUrls)),
		wg:        new(sync.WaitGroup),
		client:    m.client,
	}

	return w.do(ctx, prepareUrls(inUrls))
}

type urlInfo struct {
	position int
	str      string
	err      error
}

func prepareUrls(urls model.Urls) []urlInfo {
	urlsInfo := make([]urlInfo, 0, len(urls))

	for i, u := range urls {
		urlsInfo = append(urlsInfo, urlInfo{
			position: i,
			str:      string(u),
		})
	}

	return urlsInfo
}
