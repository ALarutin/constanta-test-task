package multiplexer

import (
	"context"
	"errors"
)

type mockDoerSuccessful struct{}

const mockRespStr = "some response"

func (*mockDoerSuccessful) Do(context.Context, string) (string, error) {
	return mockRespStr, nil
}

type mockDoerUnsuccessful struct{}

var mockDoerError = errors.New("some error")

func (*mockDoerUnsuccessful) Do(context.Context, string) (string, error) {
	return "", mockDoerError
}
