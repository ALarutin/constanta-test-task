package multiplexer

import (
	"context"
	"fmt"
	"reflect"
	"sync"
	"testing"

	"gitlab/constanta-test-task/internal/model"
)

const mockUrlStr = "some url"

func Test_worker_do(t *testing.T) {
	type fields struct {
		outCons   int
		wg        *sync.WaitGroup
		client    doer
		responses []string
	}
	type args struct {
		urls []urlInfo
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Responses
		wantErr bool
		errMsg  string
	}{
		{
			name: "Successful test",
			fields: fields{
				outCons:   4,
				wg:        new(sync.WaitGroup),
				client:    new(mockDoerSuccessful),
				responses: make([]string, 5),
			},
			args: args{urls: []urlInfo{
				{
					position: 0,
					str:      mockUrlStr,
				},
				{
					position: 1,
					str:      mockUrlStr,
				},
				{
					position: 2,
					str:      mockUrlStr,
				},
				{
					position: 3,
					str:      mockUrlStr,
				},
				{
					position: 4,
					str:      mockUrlStr,
				},
			}},
			want: model.Responses{
				mockRespStr,
				mockRespStr,
				mockRespStr,
				mockRespStr,
				mockRespStr,
			},
		},
		{
			name: "Unsuccessful test, doer error",
			fields: fields{
				outCons:   4,
				wg:        new(sync.WaitGroup),
				client:    new(mockDoerUnsuccessful),
				responses: make([]string, 1),
			},
			args: args{urls: []urlInfo{
				{
					position: 0,
					str:      mockUrlStr,
				},
			}},
			wantErr: true,
			errMsg:  fmt.Sprintf("url: %s\nerror message: %s", mockUrlStr, mockDoerError.Error()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &worker{
				outCons:   tt.fields.outCons,
				wg:        tt.fields.wg,
				client:    tt.fields.client,
				responses: tt.fields.responses,
			}
			got, err := w.do(context.Background(), tt.args.urls)
			if err != nil && !tt.wantErr {
				t.Errorf("do() error = %+v, wantErr %+v", err, tt.wantErr)
				return
			}
			if (err != nil && tt.wantErr) && err.Error() != tt.errMsg {
				t.Errorf("do() error = %+v, wantErr %s", err, tt.errMsg)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("do() got = %+v, want = %+v", got, tt.want)
			}
		})
	}
}
