package multiplexer

import (
	"context"
)

type doer interface {
	Do(ctx context.Context, url string) (string, error)
}
