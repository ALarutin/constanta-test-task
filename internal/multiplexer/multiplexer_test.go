package multiplexer

import (
	"context"
	"testing"

	"gitlab/constanta-test-task/internal/model"
)

func TestMultiplexer_Multiplex(t *testing.T) {
	type fields struct {
		cfg    Cfg
		client doer
	}
	type args struct {
		inUrls model.Urls
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		errMsg  string
	}{
		{
			name: "Unsuccessful test, not valid urls error",
			fields: fields{
				cfg: Cfg{
					MaxUrlsSize: 2,
				},
			},
			args:    args{inUrls: append(model.Urls(nil))},
			wantErr: true,
			errMsg:  model.ErrorEmptyUrls.Error(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Multiplexer{
				cfg:    tt.fields.cfg,
				client: tt.fields.client,
			}
			_, err := m.Multiplex(context.Background(), tt.args.inUrls)
			if err != nil && !tt.wantErr {
				t.Errorf("Multiplex() error = %+v, wantErr %+v", err, tt.wantErr)
				return
			}
			if (err != nil && tt.wantErr) && err.Error() != tt.errMsg {
				t.Errorf("Multiplex() error = %+v, wantErr %s", err, tt.errMsg)
				return
			}
		})
	}
}
