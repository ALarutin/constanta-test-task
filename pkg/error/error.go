package error

import (
	"errors"
)

var errorEmptyError = errors.New("empty error")

type ServerError struct {
	Err error
}

func (e ServerError) Error() string {
	return e.Err.Error()
}

func NewServerError(err error) error {
	if err == nil {
		return &ServerError{Err: errorEmptyError}
	}

	return &ServerError{Err: err}
}

type BusinessError struct {
	err error
}

func (e BusinessError) Error() string {
	return e.err.Error()
}

func NewBusinessError(err error) *BusinessError {
	if err == nil {
		return &BusinessError{err: errorEmptyError}
	}

	return &BusinessError{err: err}
}
