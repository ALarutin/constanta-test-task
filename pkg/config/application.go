package config

type (
	Application struct {
		Name        string      `json:"name"`
		Version     string      `json:"version"`
		Server      Server      `json:"server"`
		Cache       Cache       `json:"cache"`
		Client      Client      `json:"client"`
		Multiplexer Multiplexer `json:"multiplexer"`
	}
	Server struct {
		Timeout         Duration `json:"timeout"`
		ShutdownTimeout Duration `json:"shutdown_timeout"`
		MaxInputCons    int      `json:"max_http_input_cons"`
	}
	Cache struct {
		LiveDuration  Duration `json:"live_duration"`
		CleanDuration Duration `json:"clean_duration"`
	}
	Client struct {
		DoCache bool     `json:"do_cache"`
		Timeout Duration `json:"timeout"`
	}
	Multiplexer struct {
		MaxUrlsSize   uint8 `json:"max_urls_size"`
		MaxOutputCons int   `json:"max_http_output_cons"`
	}
)
