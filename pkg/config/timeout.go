package config

import (
	"errors"
	"fmt"
	"time"
)

type Duration time.Duration

var (
	errorNilPointer       = errors.New("duration is a nil pointer")
	errorInvalidatedBytes = errors.New("invalidated bytes to unquoted")
)

func (d *Duration) UnmarshalJSON(durationBytes []byte) error {
	if d == nil {
		return errorNilPointer
	}

	if len(durationBytes) == 0 || string(durationBytes) == "null" {
		return nil
	}

	durationStr, err := unquote(durationBytes)
	if err != nil {
		return err
	}

	duration, err := time.ParseDuration(durationStr)
	if err != nil {
		return fmt.Errorf("can't parse duration: %s; with error: %w", durationStr, err)
	}

	*d = Duration(duration)

	return nil
}

func (d *Duration) Cast() time.Duration {
	if d == nil {
		return 0
	}

	return time.Duration(*d)
}

func unquote(bytes []byte) (string, error) {
	if len(bytes) <= 2 {
		return "", errorInvalidatedBytes
	}

	if bytes[0] == '"' && bytes[len(bytes)-1] == '"' {
		bytes = bytes[1 : len(bytes)-1]
	}

	return string(bytes), nil
}
