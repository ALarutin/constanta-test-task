package config

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	Application Application `json:"application"`
}

func NewConfig(configFilePath string) Config {
	bytes, err := os.ReadFile(configFilePath)
	if err != nil {
		log.Panicf("can't read file by path: %s with error: %s", configFilePath, err.Error())
	}

	cfg := Config{}

	if err = json.Unmarshal(bytes, &cfg); err != nil {
		log.Panicf("can't unmarshal files bytes by path: %s with error: %s", configFilePath, err.Error())
	}

	return cfg
}
