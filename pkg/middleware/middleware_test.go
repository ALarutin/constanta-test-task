package middleware

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"sync"
	"testing"
	"time"
)

func getHandler(handlerFunc http.HandlerFunc) http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/", handlerFunc)
	return mux
}

func panicHandler(http.ResponseWriter, *http.Request) {
	panic("some panic")
}

func TestMiddleware_Recover(t *testing.T) {
	type fields struct {
		logger *log.Logger
		cfg    Cfg
		inCons int32
	}
	type args struct {
		next http.Handler
		resp *httptest.ResponseRecorder
		r    *http.Request
	}
	type expected struct {
		status int
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		want     http.Handler
		expected expected
	}{
		{
			name: "Successful catch panic",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
			},
			args: args{
				next: getHandler(panicHandler),
				resp: httptest.NewRecorder(),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusInternalServerError,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Middleware{
				logger: tt.fields.logger,
			}

			recoverMiddleware := m.Recover(tt.args.next)
			recoverMiddleware.ServeHTTP(tt.args.resp, tt.args.r)

			if tt.args.resp.Code != tt.expected.status {
				t.Errorf("got status: %d;expected: %d", tt.args.resp.Code, tt.expected.status)
			}
		})
	}
}

func defaultHandler(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func sleepHandler(http.ResponseWriter, *http.Request) {
	time.Sleep(time.Second)
}

const mockMaxInputCons = 10

func TestMiddleware_CountRequest(t *testing.T) {
	type fields struct {
		logger *log.Logger
		cfg    Cfg
		inCons int32
	}
	type args struct {
		next http.Handler
		r    *http.Request
	}
	type expected struct {
		status int
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		want     http.Handler
		expected expected
	}{
		{
			name: "Successful count",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
			},
			args: args{
				next: getHandler(defaultHandler),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusOK,
			},
		},
		{
			name: "Unsuccessful count",
			fields: fields{
				logger: log.New(os.Stderr, "", log.Llongfile),
			},
			args: args{
				next: getHandler(sleepHandler),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusTooManyRequests,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Middleware{
				logger: tt.fields.logger,
				cfg: Cfg{
					MaxInputCons: mockMaxInputCons,
				},
			}

			crMiddleware := m.CountRequest(tt.args.next)

			var (
				wgEnd   = new(sync.WaitGroup)
				wgStart = new(sync.WaitGroup)
			)

			for i := 0; i < mockMaxInputCons; i++ {
				wgEnd.Add(1)
				wgStart.Add(1)
				go func() {
					defer wgEnd.Done()
					wgStart.Done()
					crMiddleware.ServeHTTP(httptest.NewRecorder(), tt.args.r)
				}()
			}

			wgStart.Wait()

			resp := httptest.NewRecorder()

			crMiddleware.ServeHTTP(resp, tt.args.r)

			if resp.Code != tt.expected.status {
				t.Errorf("got status: %d;expected: %d", resp.Code, tt.expected.status)
				return
			}

			wgEnd.Wait()

			if m.inCons != 0 {
				t.Errorf("not 0 in cons %d", m.inCons)
			}
		})
	}
}
