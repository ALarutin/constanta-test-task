package middleware

import (
	"log"
	"net/http"
	"runtime/debug"
	"sync/atomic"
	"time"
)

type (
	Middleware struct {
		logger *log.Logger
		cfg    Cfg
		inCons int32
	}
	Cfg struct {
		Timeout      time.Duration
		MaxInputCons int
	}
)

func NewMiddleware(cfg Cfg, logger *log.Logger) *Middleware {
	return &Middleware{
		logger: logger,
		cfg:    cfg,
	}
}

func (m *Middleware) Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				m.logger.Printf("caught panic: %v\n%s", r, debug.Stack())
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) CountRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if in := atomic.LoadInt32(&m.inCons); in >= int32(m.cfg.MaxInputCons) {
			http.Error(w, "too many requests", http.StatusTooManyRequests)
			return
		}

		atomic.AddInt32(&m.inCons, 1)
		defer atomic.AddInt32(&m.inCons, -1)

		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) Timeout(next http.Handler) http.Handler {
	return http.TimeoutHandler(next, m.cfg.Timeout, "request timeout exceeded")
}
